# serve-function

Serve a function as a REST API.

## Usage
The `serve-function` module has a simple interface:

``` js
  var serve = require('serve-function');

  function functionToServe (options, callback) {
    var result = options.a + options.b + options.c
    callback(null,result)
  }

  var PORT = 9876;

  serve({
    port: PORT,
    function: functionToServe
  })

  // now use the function with:
  // http://<host>:9876/?a='a'&b='b'&c='c'

```

## Run Tests
``` bash
  $ npm test
```
