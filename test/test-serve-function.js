;(function () {
  /* global describe, it */
  'use strict'

  var expect = require('chai').expect

  var serve = require('../serve-function')
  var request = require('request')
  var testFunction = require('./test-function')

  var lomath = require('lomath')

  var _ = require('lodash')

  describe('app', function () {
    this.timeout(1000 * 40)

    describe('get', function () {
      it('should serve a function as a REST API', function (done) {
        var PORT = 53210

        var serveOptions = {
          function: testFunction,
          port: PORT
        }

        serve(serveOptions, function (error, data) {
          expect(error).to.not.be.ok

          var url = 'http://localhost:' + PORT
          url += '?a=some&b=Thing&c=Cool'

          function requestHandler (error, response, body) {
            body = JSON.parse(body)

            expect(error).to.not.be.ok
            expect(response.statusCode).to.equal(200)

            expect(body).to.equal('someThingCool')
            done()
          }

          request(url, requestHandler)
        })
      })

      it('should return an error for bad inputs', function (done) {
        var PORT = 12345

        var options = {
          function: testFunction,
          port: PORT
        }

        serve(options, function (error, data) {
          expect(error).to.not.be.ok

          var url = 'http://localhost:' + PORT
          url += '?shouldFail=true'

          function requestHandler (error, response, body) {
            expect(error).to.not.be.ok
            expect(response.statusCode).to.not.equal(200)
            done()
          }

          request(url, requestHandler)
        })
      })
    })

    describe('post', function () {
      it('should handle post data', function (done) {
        var postData = {
          anObject: {
            a: 'aa',
            b: 1,
            c: [],
            d: {}
          },
          aString: 'string!',
          aNumber: 5,
          anArray: [
            {a: 1, b: 2},
            {a: 3, b: 4}
          ]
        }

        var PORT = 9999

        var serveOptions = {
          function: testFunction,
          port: PORT
        }

        serve(serveOptions, function (error, data) {
          expect(error).to.not.be.ok

          var url = 'http://localhost:' + PORT

          var formData = lomath.flattenJSON(postData)

          expect(formData).to.not.deep.equal(postData)

          var requestOptions = {
            url: url,
            json: formData
          }

          request.post(requestOptions, postHandler)

          function postHandler (error, response, body) {
            expect(error).to.not.be.ok
            body = lomath.unflattenJSON(body)

            expect(response.statusCode).to.equal(200)

            var expectedResult = postData

            var actualResult = body

            var match = _.isEqual(expectedResult, actualResult)

            expect(match).to.be.true

            done()
          }
        })
      })
    })
  })
})()

