;(function () {
  'use strict'

  module.exports = testFunction

  /**
   *
   * @param {Object} options all function parameters
   * @param {Object} [options.data] a big parameter
   * @param {Boolean} [options.shouldFail] return an error
   * @param {Function} callback handle results
   */
  function testFunction (options, callback) {
    if (options.shouldFail) {
      callback('failing, per request')
      return
    }

    var result = Object.keys(options).reduce(function (sum, key) {
      return sum + options[key]
    }, '')

    if (options.data && JSON.stringify(options.data) !== '{}') {
      result = options.data
    }

    callback(null, result)
  }
})()

