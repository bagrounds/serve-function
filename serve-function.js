/**
 * Initializes an Express app and an HTTP server.
 *
 * Accepts a port number to serve on and an Express router object to serve.
 *
 * @module app
 */
;(function () {
  'use strict'

  /** imports */
  var express = require('express')
  var errorHandler = require('errorhandler')
  var cors = require('cors')
  var lomath = require('lomath')
  var bodyParser = require('body-parser')

  /* exports */
  module.exports = serve

  /**
   * Given a function, a list of parameters, a port number, and an endpoint,
   * serve the function as a REST API on the given port.
   *
   * @param {Object} options all function parameters
   * @param {Function} options.function to serve
   * @param {Number} options.port to serve on
   *
   * @param {Function} callback handle results
   */
  function serve (options, callback) {
    var app = applyMiddleware(express())

    var servingFunction = options.function

    app.use('/', servingRouteGenerator(servingFunction))

    app.listen(options.port)

    callback(null, app)
  }

  function servingRouteGenerator (servingFunction) {
    return function servingRoute (request, response) {
      var functionOptions = request.query

      if (request.body && JSON.stringify(request.body) !== '{}') {
        functionOptions.data = lomath.unflattenJSON(request.body)
      }

      servingFunction(functionOptions, function (error, data) {
        if (error) {
          response.status(500).send(error.message)
        } else {
          response.json(data)
        }
      })
    }
  }

  function applyMiddleware (app) {
    app.use(errorHandler())
    app.use(cors())

    var jsonParserOptions = {
      limit: 1024 * 1024 * 20,
      type: 'application/json'
    }

    app.use(bodyParser.json(jsonParserOptions))

    var urlEncodedOptions = {
      extended: true,
      limit: 1024 * 1024 * 20,
      type: 'application/json'
    }

    app.use(bodyParser.urlencoded(urlEncodedOptions))

    return app
  }
})()
